import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function Banner() {

	return(
		<Row>
			<Col className="p-5">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere.</p>
				<Button variant="primary" as={Link} to="/courses">Enroll now!</Button>
			</Col>
		</Row>
	)
};


export const Error = () => {

	return(
		<Row>
			<Col className="p-5">
				<h1>404 page not found</h1>
				<p>We are sorry but the page you are looking for does not exist.</p>
			</Col>
		</Row>
	)
};
